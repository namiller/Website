polymer build
rm -r deploy
mkdir deploy
cp -r build/unbundled/ deploy
cp -r server deploy
cp -r pages deploy
php -S localhost:8000 -t deploy
