# CMake build configuration

When working on short term development projects (such as project euler problems) I don't tend to spend much time configuring my build environment. Instead, I opt for using one-off makefiles to do the building and show-horn test cases inline with the main source. This approach makes it difficult to wrap generally usable code into libraries and I have found it ends up leading to a lot of code reuse. To avoid these issues I decided to bite the bullet and finally configure a better solution for myself.

The result is an easilly generalizable project template that uses CMake to build and test libraries and executibles. A sample of this template can be found in the github project [cmake_template](https://github.com/namiller/cmake_template). The important part of this project is not the source code, but rather the project structure and the CMakeLists.txt files which allow the build and test commands to work straightforwardly.

Here I will walk you through some of the finer points of the architecture so that you may learn something about CMake as I did, and customize the template I use to suite your needs.

## Overall structure
The top level of a project using this architecture will contain a single CMakeLists.txt file (refered to as the top level CMakeLists file), any project-wide files (README.md, .gitignore, etc) and a folder containing each of a number of logically divided subprojects. Because of the power of CMake, these projects may contain interdependencies (even circular ones). The division of the subprojects is somewhat arbitrary, but I recommend separating out sublibraries which could be integrated separately into other projects (ie offer some more general merit). This top level may also contain executible submodules and test case submodules which will compile to executibles rather than library objects.

We can then define the types of submodules that we would like to support as:

* libraries
* executibles
* tests

Each of these build products comes with different demands, so each will contain slightly different directory structures and CMakeLists.txt files.

### Libraries

Libraries are the bread and butter of this project structure and predicably have the most involved directory structure. Each library should have 3 sub folders:

* src - contains source code (.cpp) files for the library
* include - contains the interface (.h) files for the library
* test - contains unit tests for the library

In order to make all this work, there will be a CMakeLists.txt at this level, as well as one within the test file (refered to as the Library and Library Test CMakeLists files respectively).

### Executibles

Executibles contain just enough source code to build a runnable product. These should be very lightweight and should really just contain enough code to configure and deploy the logic residing within the library modules. Ideally this folder will only contain a single program file, a .cpp file with the main entry point. The will of course also come with a CMakeLists file (the Executible CMakeLists) to configure the build.

### Tests

Not to be confused with the library tests, these top level test modules are for performing integration tests between multiple subprojects and ensure everything is operating well together. The structure is nearly identical to the Executible structure although, having multiple .cpp files is more acceptible here. Like Executible submodules, Tests will just need one CMakeList (The Test CMakeLists).

## Example
Now that I have defined the overall structure of a project following this template here is an example of one such project (infact the one provided in the github sample.

<pre>
<code>
+-- CMakeLists.txt        -- Top level CMakeLists
+-- sub1/                 -- Library module (without dependencies)
|  +-- CMakeLists.txt     -- Library CMakeLists
|  +-- src/               -- Library sources (could contain multiple files)
|  |  +-- first.cpp
|  +-- include/           -- Library header files (could contain multiple files)
|  |  +-- first.h
|  +-- test/              -- Unit tests for library functionality
|  |  +-- CMakeLists.txt  -- Library Test CMakeLists
|  |  +-- main.cpp        -- unit test code
+-- sub2/                 -- Library module (which depends on sub1)
|  +-- CMakeLists.txt     -- Library CMakeLists
|  +-- src/
|  |  +-- second.cpp      -- Depends on and imports sub1/first.h
|  +-- include/
|  |  +-- first.h
|  +-- test/
|  |  +-- CMakeLists.txt  -- Library Test CMakeLists
|  |  +-- main.cpp
+-- test/                 -- Integration tests (depends on and tests sub1 and sub2)
|  +-- CMakeLists.txt     -- Test CMakeLists
|  +-- main.cpp
+-- exec1/                -- Executable module (depends on sub1 and sub2)
|  +-- CMakeLists.txt     -- Executable CMakeLists
|  +-- main.cpp           -- Only executable source file
</code>
</pre>

Hopefully that gives you a more concrete idea of what this sort of project would look like and were all the files fit together. Now lets look at the contents of the various types of CMakeLists.txt files.

## File Contents (explained)

### Top Level CMakeLists
```bash
cmake_minimum_required(VERSION 3.1)
project(demo_root) # this project name can be changed but it shouldn't really change much.

enable_testing()

## this is the only CMAKE VERSION 2.6+ feature used so removing it allows back comp.
set(CMAKE_CXX_STANDARD 14) #use c++14
#set(CMAKE_CXX_STANDARD 11) #use c++11

# the following variables can be changed to effect the location of the build products.
set(INCLUDE_OUTPUT_PATH "${PROJECT_BINARY_DIR}/include")
set(EXECUTABLE_OUTPUT_PATH "${PROJECT_BINARY_DIR}/bin")
set(LIBRARY_OUTPUT_PATH "${PROJECT_BINARY_DIR}/lib")
set(TEST_OUTPUT_PATH "${PROJECT_BINARY_DIR}/tests")

#must be set elsewhere for subprojects to compile independently
set(GENERATED_INCLUDE_PATH "${INCLUDE_OUTPUT_PATH}")

#TODO: change the following lines to add the correct submodule directories
add_subdirectory(sub1)  # this (sub1) must match the name of the directory (not necisarilly the name of the project)
add_subdirectory(sub2)  # ditto
add_subdirectory(exec1) # ditto
add_subdirectory(test)  # ditto
```

### Library CMakeLists

```bash
cmake_minimum_required(VERSION 3.1)

get_filename_component(CUR_DIR ${CMAKE_CURRENT_SOURCE_DIR} NAME)
project(${CUR_DIR}) # set project name to directory name.

SET(PROJECT_INCLUDE_DIR "${PROJECT_SOURCE_DIR}/include")
SET(PROJECT_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/src")

SET(PROJECT_UNDER_TEST "${PROJECT_NAME}") #used by the included test to determine link deps.

# This copies the include/ header files into a folder with the project name under the output include path
file(COPY "${PROJECT_INCLUDE_DIR}/" DESTINATION "${INCLUDE_OUTPUT_PATH}/${PROJECT_NAME}")

# automatically depends on all cpp files in source directory
file(GLOB SRCS ${PROJECT_SOURCE_DIR}/*.cpp)

include_directories(${PROJECT_INCLUDE_DIR} ${GENERATED_INCLUDE_PATH})
add_library(${PROJECT_NAME} SHARED ${SRCS})

#target_link_libraries(${PROJECT_NAME} dep) #TODO: uncomment and change dep -> whatever subproject dependency(s) this has.

add_subdirectory(test)
```


### Library Test CMakeLists

```bash
cmake_minimum_required(VERSION 3.1)
project(${PROJECT_UNDER_TEST}_test)

find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

set(EXECUTABLE_OUTPUT_PATH "${TEST_OUTPUT_PATH}")

add_executable(${PROJECT_NAME} main.cpp)

target_link_libraries(${PROJECT_NAME} ${GTEST_LIBRARIES} pthread ${PROJECT_UNDER_TEST})
add_test(NAME ${PROJECT_UNDER_TEST}_unit COMMAND ${PROJECT_NAME})
```


### Test CMakeLists

```bash
cmake_minimum_required(VERSION 3.1)
project(integration_test)

find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS} ${GENERATED_INCLUDE_PATH})

set(EXECUTABLE_OUTPUT_PATH "${TEST_OUTPUT_PATH}") #output to the correct dir

add_executable(${PROJECT_NAME} main.cpp) #TODO: change this if the test file changes

target_link_libraries(${PROJECT_NAME} ${GTEST_LIBRARIES} pthread sub1 sub2) #TODO:change these references to the other subprojects (name is of project, not of folder)
add_test(NAME integration_test COMMAND ${PROJECT_NAME})
```

### Executable CMakeLists

```bash
cmake_minimum_required(VERSION 3.1)

get_filename_component(CUR_DIR ${CMAKE_CURRENT_SOURCE_DIR} NAME)
project(${CUR_DIR}) # set project name to directory name.

set(PROJECT_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}")

set(SRCS
  ${PROJECT_SOURCE_DIR}/main.cpp
)

# here I include the output path so that the code has the correct import paths that a user of the
# library would be using (assuming they have the generated include/ folder in their search path.
include_directories("${GENERATED_INCLUDE_PATH}")

add_executable(${PROJECT_NAME} ${SRCS})
target_link_libraries(${PROJECT_NAME} sub1 sub2) #TODO: sub1, sub2 should be exchanged for the real deps.
```

## Conclusion
With that, we are able to compile and test our projects with ease. The killer feature of this architecture for me, is that  the projects can be compiled independently with only a small amount of additional work. Additionally, the submodule makefiles are totally interchangeable so if you are working on multiple projects that share a submodule, one project may add a symbolically linked submodule pointing to the shared module in the other project, which removes the temptation to duplicate code.

## Note
It is important to note that since the package name is inferred from the folder name, certain keywords should be avoided (such as debug).
