<?php

$db = file_get_contents("projects.json");
$response = json_decode($db, true);

$tag_filter = $_GET['tag'];
$state_filter = $_GET['state'];
$range = $_GET['range'];

if (isset($tag_filter)) {
  foreach ($response as $key => $project) {
    if (!in_array($tag_filter, $project['tags'])) {
      unset($response[$key]);
    }
  }
}

if (isset($state_filter)) {
  foreach ($response as $key => $project) {
    if ($project['state'] != $state_filter) {
      unset($response[$key]);
    }
  }
}

if (isset($range)) {
  $rvals = explode(',', $range);
  $start = intval($rvals[0]);
  $end = intval($rvals[1]);
  $response = array_slice($response, $start, $end-$start);
}

echo json_encode(array_values($response));

?>
