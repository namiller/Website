<?php

$db = file_get_contents("projects.json");
$db2 = file_get_contents("../resources/interests.json");
$projects = json_decode($db, true);
$interests = json_decode($db2, true);

$tags = array();

foreach ($projects as $name => $props) {
  foreach ($props["tags"] as $tag) {
    $tags[$tag] = true;
  }
}
foreach ($interests as $name => $props) {
    $tags[$props["category"]] = true;
}
$ret = array();
foreach ($tags as $tag => $dump) {
  array_push($ret, $tag);
}

echo json_encode($ret);

?>
